
== Creating a PCB

=== Basic PCB concepts

A printed circuit board in KiCad is generally made up of *footprints* representing electronic
components and their pads, *nets* defining how those pads connect to each other, *tracks*, *vias*,
and *filled zones* that form the copper connections between pads in each net, and various graphic
shapes defining the board edge, silkscreen markings, and any other desired information.

KiCad normally keeps the information about nets on a PCB synchronized with an associated
schematic, but nets can also be created and edited directly within the PCB editor.

=== Capabilities

KiCad is capable of creating printed circuit boards with up to 32 copper layers, 14 technical
layers (silkscreen, solder mask, component adhesive, solder paste, etc) and 13 general-purpose
drawing layers.

The internal measurement resolution of all objects in KiCad is 1 nanometer, and measurements are
stored as 32-bit integers.  This means it is possible to create boards up to appproximately
4 meters by 4 meters.

KiCad currently supports one board file per project / schematic.

=== Starting from a schematic

Creating a board from a schematic is the recommended workflow for KiCad.  When you create a new
project, KiCad will generate an empty board file with the same name as the project.  To start
designing the board after you have created a schematic, simply open the board file.  You can do
this either from the KiCad project manager, or by clicking the "Open PCB in board editor" button in
the schematic editor.

=== Starting from scratch

It is also possible to create a board with no matching schematic, although this workflow has some
limitations and is not recommended for most users.  To do this, you must start the PCB editor
standalone (not from the KiCad project manager).  Before beginning your design, it is a good idea
to save the board file, which will also create a project file to store board settings.  Use "Save
As..." from the File menu to choose where to save your board file.  A project file with the same
name will be created in the same location you choose to save the board file in.

=== Board setup

Before beginning your board design, use the Board Setup dialog to configure the basic parameters of
the board.  To open Board Setup, click the image:images/icons/options_board.png[] icon in the top
toolbar or choose "Board Setup..." from the File menu.

==== Configuring board stackup and physical parameters

There are two sections of Board Setup used to configure the stackup and layers of the board. The
Board Editor Layers section is used to enable or disable technical (non-copper) layers, and give
custom names to layers if desired. The Physical Stackup section is used to configure the number of
copper layers, as well as the physical parameters of the copper and dielectric layers such as
thickness and material type.

To configure the board stackup, start on the Physical Stackup section:

image::images/pcbnew_board_setup_physical_stackup.png[scaledwidth="70%"]

Set the number of copper layers in the upper left corner, and then enter the physical parameters of
the stackup if desired.  These parameters may be left at their default values, but note that the
board thickness value will be used when exporting a 3D model of the board, so if you plan to use
this feature, it is a good idea to ensure that the stackup thickness is correct.

NOTE: KiCad currently only supports stackups with an even number of copper layers. To create
      designs with an odd number of layers (for example, flexible printed circuits or metal-core
      printed circuits), simply choose the next highest even number and ignore the extra layer.

Next, if desired, use the Board Editor Layers section to rename layers or hide non-copper layers
that you will not be using in the design.  For example, if you will not use a back silkscreen on
the design, uncheck the box next to the `B.Silkscreen` layer.

image::images/pcbnew_board_setup_board_editor_layers.png[scaledwidth="70%"]

NOTE: Copper layers can be designated as signal, power plane, mixed, or jumper in the Board Editor
      Layers section.  This designation is intended as a guide for the user only. Tracks and zones
      can be routed on any copper layer, no matter what the type is configured to in this dialog.

Some additional board stackup settings are found on the Board Finish and Solder Mask/Paste sections
of the Board Setup dialog.  The Board Finish section has settings for defining the copper finish
and special features such as castellations or edge plating.  Note that these settings only impact
the board attributes output as part of Gerber job files at this time.

The Solder Mask/Paste section allow global adjustment of the clearance (positive or negative)
between the copper shapes and solder mask / solder paste shapes of pads on the board.  These values
will be added to any clearance overrides set on individual footprints or pads.  Positive clearance
values will result in the shape of the solder mask or paste opening being _larger_ than the copper
shape. Negative clearance values will result in the opening being _smaller_ than the copper shape.

WARNING: Most commercial PCB fabricators expect these values to be zero, and make their own
         adjustments to solder mask and paste openings as part of their CAM process. It is usually
         best to leave these values at their default of zero unless you are making the PCB yourself
         or have specific advice from your fabricator to use different values.

==== Configuring default text and graphic settings

The Text & Graphics Defaults section of the Board Setup dialog can be used to configure the
properties that will be used for new text and graphic shapes that are placed on the board.

image::images/pcbnew_board_setup_text_and_graphics.png[scaledwidth="70%"]

Line thickness, text size, and text appearance can be configured for the six different categories
of layers shown in the dialog.  Additionally, the properties for dimension objects can be
configured for all layers.  For more details about dimension properties, see the Dimensions section
below.

Text replacement variables can be created in the Text Variables section.  These variables allow
you to substitute the variable name for any text string. This substitution happens anywhere the
variable name is used inside the variable replacement syntax of `${VARIABLENAME}`.

For example, you could create a variable named `VERSION` and set the text substitution to `1.0`.
Now, in any text object on the PCB, you can enter `${VERSION}` and KiCad will substitute `1.0`. If
you change the substitution to `2.0`, every text object that includes `${VERSION}` will be updated
automatically. You can also mix regular text and variables. For example, you can create a text
object with the text `Version: ${VERSION}` which will be substituted as `Version: 1.0`.

==== Configuring design rules

Design rules control the behavior of the interactive router, the filling of copper zones, and the
design rule checker.  Design rules can be modified at any time, but we recommend that you establish
all known design rules at the beginning of the board design process.

===== Constraints

Basic design rules are configured in the Constraints section of the Board Setup dialog.
Constraints in this section apply to the entire board, and should be set to the values recommended
by your board manufacturer.  Any minimum value set here is an _absolute_ minimum and cannot be 
overridden with a more specific design rule.  For example, if you need the copper clearance on part
of a board to be 0.2mm, and in the rest 0.3mm, you must enter 0.2mm for the minimum copper
clearance in the Constraints section and use a netclass or custom rule to set the larger 0.3mm
clearance.

image::images/pcbnew_board_setup_constraints.png[scaledwidth="70%"]

In addition to setting minimum clearances, a number of features can be configured here:

[options="header",cols="25%,75%"]
|====
| Setting | Description
| Allow blind/buried vias
  | This setting must be enabled before you can place blind or buried vias using the router.
    Blind vias are mechanically-drilled vias that start at one of the outer copper layers but end
    at one of the inner layers.  Buried vias are mechanically-drilled vias that start and end at
    inner copper layers.
| Allow micro vias
  | This setting must be enabled before you can place microvias using the router.  Microvias are
    typically laser-drilled vias that connect an outer copper layer to the adjacent inner layer.
    KiCad supports a separate size constraint for microvias as they typically can be smaller than
    mechanically-drilled vias.
| Arc/circle approximated by segments
  | In some situations, KiCad must use a series of straight line segments to approximate round
    shapes such as those of arcs and circles.  This setting controls the maximum error allowed by
    this approximation: in other words, the maximum distance between a point on one of these line
    segments and the true shape of the arc or circle.  Setting this to a lower number than the
    default value of 0.005mm will result in smoother shapes, but can be very slow on larger boards.
    The default value typically results in arc approximation error that is not detectable in the
    manufactured board due to manufacturing tolerances.
| Zone fill strategy
  | KiCad's zone filling algorithm has been improved to give better results and faster performance.
    The new algorithm produces slightly different results from the old algorithm, so this setting
    allows the old behavior to be preserved to prevent producing different Gerber outputs when
    opening an old design in the latest version of KiCad. We recommend using the Smoothed Polygons
    mode for all new designs.
| Allow fillets outside zone outline
  | Zones can have fillets (rounded corners) added in the Zone Properties dialog.  By default, no
    zone copper, including fillets, is allowed outside the zone outline.  This effectively means
    that inside corners of the zone outline will not be filleted even when a fillet is configured.
    By enabling this setting, inside corners of the zone outline will be filleted even though this
    results in copper from the zone extending outside the zone outline.
| Include stackup height in track length calculations
  | By default, the height of the stackup is used to calculate the additional length of a track
    that travels through vias from one layer to another.  This calculation relies on the board
    stackup height being correctly configured.  In some situations, it is preferable to ignore the
    height of vias and just calculate the track length assuming that vias add no length. Disabling
    this setting will exclude via length from track length calculations.
|====

===== Pre-defined Sizes

The pre-defined sizes section allows you to define the track and via dimensions you want to have
available while routing tracks. Net classes can be used to define the default dimensions for tracks
and vias in different nets (see below) but defining a list of sizes in this section will allow you
to step through these sizes while routing. For example, you may want the default track width on a
board to be 0.2 mm, but use 0.3 mm for some sections that carry more current, and 0.15 mm for some
sections where space is limited. You can define each of these track widths in the Board Setup
dialog, and then switch between them when routing traces.

image::images/pcbnew_board_setup_predefined_sizes.png[scaledwidth="70%"]

===== Net Classes

NOTE: TODO: Write this section

===== Custom Rules

NOTE: TODO: Write this section

See Custom Design Rules in the Advanced Topics chapter for more information

===== Violation Severity

NOTE: TODO: Write this section

===== Importing settings

You can import part or all of the board setup from an existing board.  This technique can be used
to create a "template" board that has the settings you want to use on multiple designs, and then
importing these settings from the template board into each new board rather than entering them
manually.

image::images/pcbnew_board_setup_import_settings.png[scaledwidth="70%"]

To import settings, click the Import Settings from Another Board... button at the bottom of the
Board Setup dialog, then choose the `kicad_pcb` file you want to import from.  Select which
settings you want to import, and the current settings will be overwritten with the values from the
chosen board.
